/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Peserta;

/**
 *
 * @author basisdata
 */
public interface Peserta {
    public String getJenisSertifikat();
    public String getFasilitas();
    public String getKonsumsi();
}
