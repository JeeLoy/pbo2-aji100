/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Penduduk;

import Peserta.Peserta;

/**
 *
 * @author basisdata
 */
public class Mahasiswa extends Penduduk implements Peserta {

    private String nim;

    public Mahasiswa(String nama, String tanggalLahir) {
        super(nama, tanggalLahir);
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    @Override
    public double getIuran() {
        return Integer.parseInt(nim) / 10000;
    }

    @Override
    public String getJenisSertifikat() {
        return "Panitia";
    }

    @Override
    public String getFasilitas() {
        return "Block Note, Alat Tulis, dan Laptop";
    }

    @Override
    public String getKonsumsi() {
        return "Snack, Makan Siang, dan Makan Malam";
    }

}
