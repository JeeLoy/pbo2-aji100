/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Penduduk;

import Peserta.Peserta;

/**
 *
 * @author basisdata
 */
public class Masyarakat extends Penduduk implements Peserta{

    private String nomor;

    public Masyarakat(String nama, String tanggalLahir) {
        super(nama, tanggalLahir);
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    @Override
    public double getIuran() {
        return Integer.parseInt(nomor) * 100;
    }

    @Override
    public String getJenisSertifikat() {
        return "Peserta";
    }

    @Override
    public String getFasilitas() {
        return "Block Note, Alat Tulis, dan Modul Pelatihan";
    }

    @Override
    public String getKonsumsi() {
        return "Snack dan Makan Siang";
    }
}
