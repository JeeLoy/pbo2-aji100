/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Penduduk.Mahasiswa;
import Penduduk.Masyarakat;
import Penduduk.Penduduk;
import UKM.UKM;
import java.util.Arrays;

/**
 *
 * @author basisdata
 */
public class Main {

    public static void main(String[] args) {
        Penduduk[] pdd = new Penduduk[5];

        UKM ukm = new UKM();
        ukm.setNamaUnit("Pelatihan Internet");

        Mahasiswa mhs = new Mahasiswa("Edo", "20 Mei 1999");
        mhs.setNim("125314040");
        pdd[0] = mhs;
        ukm.setKetua(mhs);

        mhs = new Mahasiswa("Atma", "01 Des 2000");
        mhs.setNim("125314010");
        pdd[1] = mhs;
        ukm.setSekretaris(mhs);

        Masyarakat mkt = new Masyarakat("Aji", "05 July 1997");
        mkt.setNomor("102");
        pdd[2] = mkt;

        mkt = new Masyarakat("Bayu", "29 Aug 1998");
        mkt.setNomor("101");
        pdd[3] = mkt;

        mhs = new Mahasiswa("Vandi", "22 Des 1999");
        mhs.setNim("125314009");
        pdd[4] = mhs;

        System.out.println("Nama UKM\t: " + ukm.getNamaUnit());
        System.out.println("Nama Ketua\t: " + ukm.getKetua().getNama());
        System.out.println("Nama Sekretaris\t: " + ukm.getSekretaris().getNama());

        System.out.printf("================================================================================================================================\n"
                + "No. " + " Nama\t" + "Iuran\t" + " Sertifikat\t" + "Fasilitas\t\t\t\t\t" + "Konsumsi\n"
                + "-----------------------------------------------------------------------------------------------------------------------------\n");
        for (int i = 0; i < pdd.length; i++) {
            System.out.print(i + 1 + ".   " + pdd[i].getNama() + "\t");

            if (pdd[i] instanceof Mahasiswa) {
                if (pdd[i] == ukm.getKetua() || pdd[i] == ukm.getSekretaris()) {
                    System.out.print("-\t ");
                } else {
                    System.out.print(((Mahasiswa) pdd[i]).getIuran() + "\t ");
                }
                System.out.println(((Mahasiswa) pdd[i]).getJenisSertifikat() + " \t"
                        + ((Mahasiswa) pdd[i]).getFasilitas() + "\t\t"
                        + ((Mahasiswa) pdd[i]).getKonsumsi());
            } else if (pdd[i] instanceof Masyarakat) {
                System.out.println(((Masyarakat) pdd[i]).getIuran() + "\t "
                        + ((Masyarakat) pdd[i]).getJenisSertifikat() + " \t"
                        + ((Masyarakat) pdd[i]).getFasilitas() + "\t"
                        + ((Masyarakat) pdd[i]).getKonsumsi());
            }
        }
    }
}
